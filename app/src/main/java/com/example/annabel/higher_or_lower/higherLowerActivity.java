package com.example.annabel.higher_or_lower;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class higherLowerActivity extends AppCompatActivity {

    private int currentScore = 0;
    private int highScore;
    private int previousRoll = 3;
    private int roll;
    private TextView vCurrentScore;
    private TextView vHighScore;
    private ListView vThrowList;
    private ImageView vDiceImage;
    private Button vLower;
    private Button vHigher;
    private int[] diceImages;
    private ArrayAdapter arrayAdapter;
    private List<String> throwList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_higher_lower);
        
        vCurrentScore = findViewById(R.id.currentScoreNumber);
        vHighScore = findViewById(R.id.highScoreNumber);
        vThrowList = findViewById(R.id.highScoresList);
        vDiceImage = findViewById(R.id.diceImage);
        vLower = findViewById(R.id.lowerButton);
        vHigher = findViewById(R.id.higherButton);

        throwList = new ArrayList<String>();
        diceImages = new int[]{R.drawable.d1,R.drawable.d2,R.drawable.d3,R.drawable.d4,R.drawable.d5,R.drawable.d6};
        vDiceImage.setImageResource(diceImages[2]); //start with 3

        vLower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roll = rollDice();
                if (previousRoll<roll){
                    Toast.makeText(getApplicationContext(), "fout", Toast.LENGTH_SHORT).show();
                    finalScore();
                }  else if (previousRoll>roll){
                    Toast.makeText(getApplicationContext(), "goed", Toast.LENGTH_SHORT).show();
                    currentScore ++;
                }  else{
                    Toast.makeText(getApplicationContext(), "zelfde", Toast.LENGTH_SHORT).show();
                }
                vCurrentScore.setText(Integer.toString(currentScore));
                updateUI();
            }
        });

        vHigher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roll = rollDice();
                if (previousRoll>roll){
                    Toast.makeText(getApplicationContext(), "fout", Toast.LENGTH_SHORT).show();
                    finalScore();
                }  else if (previousRoll<roll){
                    Toast.makeText(getApplicationContext(), "goed", Toast.LENGTH_SHORT).show();
                    currentScore++;
                }  else{
                    Toast.makeText(getApplicationContext(), "zelfde", Toast.LENGTH_SHORT).show();
                }
                updateUI();
            }
        });
    }

    private int rollDice(){
        int number = (int)(Math.random()*6) +1;
        vDiceImage.setImageResource(diceImages[number-1]);
        return number;
    }
    private void finalScore(){
        if (currentScore > highScore){
            highScore = currentScore;
            vHighScore.setText(Integer.toString(highScore));
        }
        currentScore = 0;
    }

    private void updateUI() {
        vCurrentScore.setText(Integer.toString(currentScore));
        throwList.add("Throw is: " + Integer.toString(roll));
        if (throwList.size()>4){throwList.remove(0);}
        if (arrayAdapter == null) {
            arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, throwList);
            vThrowList.setAdapter(arrayAdapter);
        } else {
            arrayAdapter.notifyDataSetChanged();
        }
        previousRoll = roll;
    }
}
